#!/bin/sh

# Copyright (c) 2021 Qualcomm Innovation Center, Inc. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#
#     * Neither the name of Qualcomm Innovation Center, Inc. nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

CURDIR=$(cd `dirname $0` ; pwd)

cd ${LEGATO_ROOT} && source ${LEGATO_ROOT}/bin/configlegatoenv

TARGET_STAGE_DIR=${LEGATO_ROOT}/build/${TARGET}/_staging_system.${TARGET}.update_ro/

for full_name_prop in `find ${TELAF_PROP} -type f -name "*.so"`
do
    base_name=`basename ${full_name_prop}`
    full_name_telaf=`find ${TARGET_STAGE_DIR} -type f -name ${base_name}`
    if [ -n "${full_name_telaf}" ]; then
        cp -rf ${full_name_prop} ${full_name_telaf}
    fi
done

for full_name_internal in `find ${TELAF_INTERNAL} -type f -name "*.so"`
do
    base_name=`basename ${full_name_internal}`
    full_name_telaf=`find ${TARGET_STAGE_DIR} -type f -name ${base_name}`
    if [ -n "${full_name_telaf}" ]; then
        cp -rf ${full_name_internal} ${full_name_telaf}
    fi
done

mklegatoimg -t "${TARGET}" \
            -d "${TARGET_STAGE_DIR}/" \
            -o "${OUTPUT}"
